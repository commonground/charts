# Common Ground charts

This repository contains some of the Helm charts that are available for Common Ground.
Its pipeline is also triggered by other Common Ground projects to release Helm Charts not in this repository.

Repositories triggering this pipeline:

- FSC NLX
- NLX
- NLX Demo
- OpenFSC

All of these charts are combined and listed in https://charts.commonground.nl/index.yaml

## Usage

Add the repo as follows:

```bash
helm repo add commonground https://charts.commonground.nl/
helm repo update
```

Then, for example, install the Haven dashboard:

```bash
helm install haven-dashboard commonground/haven-dashboard
```

## Licence
Copyright © 2020 VNG Realisatie<br />
Licenced under EUPL v1.2
